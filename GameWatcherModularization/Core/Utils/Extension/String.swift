//
//  String.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 30/12/22.
//

import Foundation

extension String {
    
    var removingHTMLOccurances: String {
        let newLine = self.replacingOccurrences(of: "<br />", with: "\n", options: .regularExpression, range: nil)
        return newLine.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
}
