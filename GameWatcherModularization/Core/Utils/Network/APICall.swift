//
//  APICall.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import Foundation

struct API {
    
    static let baseUrl = "https://api.rawg.io/api/"
    
    static var apiKey: String {
        guard let filePath = Bundle.main.path(forResource: "RAWG-Info", ofType: "plist") else {
            fatalError("Couldn't find file 'RAWG-Info.plist'.")
        }
        
        let plist = NSDictionary(contentsOfFile: filePath)
        guard let value = plist?.object(forKey: "API_KEY") as? String else {
            fatalError("Couldn't find key 'API_KEY' in 'RAWG-Info.plist'.")
        }
        return value
    }
    
}

protocol Endpoint {
    
    var url: String { get }
    
}

enum Endpoints {
    
    enum Gets: Endpoint {
        case platforms
        case gameDetail
        case gameDetails
        case search
        
        public var url: String {
            switch self {
            case .platforms: return "\(API.baseUrl)platforms?key=\(API.apiKey)"
            case .gameDetail: return "\(API.baseUrl)games/"
            case .gameDetails: return "\(API.baseUrl)games?key=\(API.apiKey)&platforms="
            case .search: return "\(API.baseUrl)games?key=\(API.apiKey)&search="
            }
        }
    }
    
}
