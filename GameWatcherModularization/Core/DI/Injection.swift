//
//  Injection.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import UIKit
import Platform
import Core
import GameDetails

final class Injection: NSObject {
    
    func providePlatform<U: UseCase>() -> U where U.Request == Any, U.Response == [PlatformModel] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let locale = GetPlatformsLocaleDataSource(realm: appDelegate.realm)
        
        let remote = GetPlatformsRemoteDataSource(endpoint: Endpoints.Gets.platforms.url)
        
        let mapper = PlatformTransformer()
        
        let repository = GetPlatformsRepository(
            localeDataSource: locale,
            remoteDataSource: remote,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideGameDetails<U: UseCase>() -> U where U.Request == String, U.Response == [GameDetailModel] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let locale = GetGameDetailsLocaleDataSource(realm: appDelegate.realm)
        
        let remote = GetGameDetailsRemoteDataSource(endpoint: Endpoints.Gets.gameDetails.url)
        
        let gameDetailsMapper = GameDetailTransformer()
        let mapper = GameDetailsTransformer(gameDetailMapper: gameDetailsMapper)
        
        let repository = GetGameDetailsRepository(
            localeDataSource: locale,
            remoteDataSource: remote,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideSearch<U: UseCase>() -> U where U.Request == String, U.Response == [GameDetailModel] {
        let remote = GetGameDetailsRemoteDataSource(endpoint: Endpoints.Gets.search.url)
        
        let gameDetailsMapper = GameDetailTransformer()
        let mapper = GameDetailsTransformer(gameDetailMapper: gameDetailsMapper)
        
        let repository = SearchGameDetailRepository(
            remoteDataSource: remote,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideGameDetail<U: UseCase>() -> U where U.Request == String, U.Response == GameDetailModel {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let locale = GetGameDetailsLocaleDataSource(realm: appDelegate.realm)
        
        let remote = GetGameDetailRemoteDataSource(endpoint: Endpoints.Gets.gameDetail.url)
        
        let mapper = GameDetailTransformer()
        
        let repository = GetGameDetailRepository(
            localeDataSource: locale,
            remoteDataSource: remote,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideUpdateFavorite<U: UseCase>() -> U where U.Request == String, U.Response == GameDetailModel {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let locale = GetFavoriteGameDetailLocaleDataSource(realm: appDelegate.realm)
        
        let mapper = GameDetailTransformer()
        
        let repository = UpdateFavoriteGameDetailRepository(
            localeDataSource: locale,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
    
    func provideFavorite<U: UseCase>() -> U where U.Request == String, U.Response == [GameDetailModel] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let locale = GetFavoriteGameDetailLocaleDataSource(realm: appDelegate.realm)
        
        let gameDetailsMapper = GameDetailTransformer()
        let mapper = GameDetailsTransformer(gameDetailMapper: gameDetailsMapper)
        
        let repository = GetFavoriteGameDetailsRepository(
            localeDataSource: locale,
            mapper: mapper)
        
        return Interactor(repository: repository) as! U
    }
}
