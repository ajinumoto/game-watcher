//
//  CustomEmptyView.swift
//  CleanGameWatcher
//
//  Created by Nexsoft on 12/12/22.
//

import SwiftUI

struct CustomEmptyView: View {
    var image: String
    var title: String
    
    var body: some View {
        VStack(spacing: 20) {
            Image(image)
                .resizable()
                .renderingMode(.original)
                .scaledToFit()
                .frame(width: 250)
            
            Text(title)
                .font(.system(.body, design: .rounded))
        }
    }
}
