//
//  TabItem.swift
//  CleanGameWatcher
//
//  Created by Nexsoft on 12/12/22.
//

import SwiftUI

struct TabItem: View {
    
    var imageName: String
    var title: String
    var body: some View {
        VStack {
            Image(systemName: imageName)
            Text(title)
        }
    }
}
