//
//  ContentView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 26/12/22.
//

import SwiftUI
import Platform
import GameDetails
import Core

struct ContentView: View {
    
    @EnvironmentObject var homePresenter: GetListPresenter<Any, PlatformModel, Interactor<Any, [PlatformModel], GetPlatformsRepository<GetPlatformsLocaleDataSource, GetPlatformsRemoteDataSource, PlatformTransformer>>>
    @EnvironmentObject var favoritePresenter: GetListPresenter<String, GameDetailModel, Interactor<String, [GameDetailModel], GetFavoriteGameDetailsRepository<GetFavoriteGameDetailLocaleDataSource, GameDetailsTransformer<GameDetailTransformer>>>>
    @EnvironmentObject var searchPresenter: SearchPresenter<GameDetailModel, Interactor<String, [GameDetailModel], SearchGameDetailRepository<GetGameDetailsRemoteDataSource, GameDetailsTransformer<GameDetailTransformer>>>>
    
    var body: some View {
        TabView {
            NavigationView {
                HomeView(presenter: homePresenter)
            }.tabItem {
                TabItem(imageName: "house", title: "Home")
            }
            
            NavigationView {
                SearchView(presenter: searchPresenter)
            }.tabItem {
                TabItem(imageName: "magnifyingglass", title: "Search")
            }
            
            NavigationView {
                FavoriteView(presenter: favoritePresenter)
            }.tabItem {
                TabItem(imageName: "star", title: "Favorite")
            }
            
            NavigationView {
                ProfileView()
            }.tabItem {
                TabItem(imageName: "info.circle", title: "About")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
