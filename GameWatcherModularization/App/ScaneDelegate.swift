//
//  ScaneDelegate.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import UIKit
import SwiftUI
import RealmSwift
import Core
import Platform
import GameDetails

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        let injection = Injection()
        
        let favoriteUseCase: Interactor<
            String,
            [GameDetailModel],
            GetFavoriteGameDetailsRepository<
                GetFavoriteGameDetailLocaleDataSource,
                GameDetailsTransformer<GameDetailTransformer>>
        > = injection.provideFavorite()
        
        let searchUseCase: Interactor<
            String,
            [GameDetailModel],
            SearchGameDetailRepository<
                GetGameDetailsRemoteDataSource,
                GameDetailsTransformer<GameDetailTransformer>>
        > = injection.provideSearch()
        
        let platformUseCase: Interactor<
            Any,
            [PlatformModel],
            GetPlatformsRepository<
                GetPlatformsLocaleDataSource,
                GetPlatformsRemoteDataSource,
                PlatformTransformer>
        > = injection.providePlatform()
        
        let homePresenter = GetListPresenter(useCase: platformUseCase)
        let favoritePresenter = GetListPresenter(useCase: favoriteUseCase)
        let searchPresenter = SearchPresenter(useCase: searchUseCase)
        
        let contentView = ContentView()
            .environmentObject(homePresenter)
            .environmentObject(favoritePresenter)
            .environmentObject(searchPresenter)
        
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            window.rootViewController = UIHostingController(rootView: contentView)
            self.window = window
            window.makeKeyAndVisible()
        }
    }
    
}
