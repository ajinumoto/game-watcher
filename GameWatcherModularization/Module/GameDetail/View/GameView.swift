//
//  GameView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import SDWebImageSwiftUI
import Core
import GameDetails

struct GameView: View {
    
    @State private var showingAlert = false
    
    @ObservedObject var presenter: GameDetailPresenter<
        Interactor<String, GameDetailModel, GetGameDetailRepository<GetGameDetailsLocaleDataSource, GetGameDetailRemoteDataSource, GameDetailTransformer>>,
        Interactor<String, GameDetailModel, UpdateFavoriteGameDetailRepository<GetFavoriteGameDetailLocaleDataSource, GameDetailTransformer>>
    >
    
    var game: GameDetailModel
    
    private let spacing: CGFloat = 30
    private let columns: [GridItem] = [
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var body: some View {
        ZStack {
            if presenter.isLoading {
                loadingIndicator
            } else if presenter.isError {
                errorIndicator
            } else {
                ScrollView(.vertical) {
                    VStack {
                        imageGame
                        menuButtonGame
                        content
                    }
                }
            }
        }
        .onAppear {
            self.presenter.getGameDetail(request: game.id)
        }
        .alert(isPresented: $showingAlert) {
            Alert(
                title: Text("Oops!"),
                message: Text("Something wrong!"),
                dismissButton: .default(Text("OK"))
            )
        }
        .edgesIgnoringSafeArea(.top)
        .navigationBarTitleDisplayMode(.inline)
    }
    
}

extension GameView {
    
    var loadingIndicator: some View {
        VStack {
            Text("Loading...")
            ActivityIndicator()
        }
    }
    
    var errorIndicator: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: presenter.errorMessage
        ).offset(y: 80)
    }
    
    var menuButtonGame: some View {
        HStack(alignment: .center) {
            if !(self.presenter.gameDetails?.website.isEmpty ?? true) {
                Spacer()
                CustomIcon(
                    imageName: "link.circle",
                    title: "Source"
                ).onTapGesture {
                    self.openUrl(self.presenter.gameDetails?.website)
                }
            }
            Spacer()
            if presenter.gameDetails?.favorite == true {
                CustomIcon(
                    imageName: "star.fill",
                    title: "Favorited"
                ).onTapGesture { self.presenter.updateFavoriteGameDetail(request: game.id) }
            } else {
                CustomIcon(
                    imageName: "star",
                    title: "Favorite"
                ).onTapGesture { self.presenter.updateFavoriteGameDetail(request: game.id) }
            }
            Spacer()
        }.padding()
    }
    
    var imageGame: some View {
        
        WebImage(url: URL(string: self.presenter.gameDetails?.backgroundImage ?? ""))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFill()
            .overlay(
                LinearGradient(gradient: Gradient(colors: [Color.theme.background.opacity(0.3),
                                                           Color.theme.background]),
                               startPoint: .top,
                               endPoint: .bottom)
            )
            .overlay(
                VStack(alignment: .leading) {
                    Spacer()
                    HStack {
                        Text(self.presenter.gameDetails?.name ?? "Unknown")
                            .foregroundColor(.theme.accent)
                            .textCase(.uppercase)
                            .font(.title.bold())
                            .shadow(color: .theme.background, radius: 4)
                            .lineLimit(1)
                        Spacer()
                    }
                    Text(self.presenter.gameDetails?.genreString ?? ""
                    )
                }
                    .offset(y: -20)
                    .padding()
                    .frame(width: UIScreen.main.bounds.width, height: 200.0)
            )
            .frame(width: UIScreen.main.bounds.width, height: 200.0)
    }
    
    var content: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("Description")
                .font(.headline)
            Text(presenter.gameDetails?.gameDetailsDescription.removingHTMLOccurances ?? "No Information")
                .font(.system(size: 16))
            Text("Genres")
                .font(.headline)
            Text(presenter.gameDetails?.genreString ?? "-")
                .font(.system(size: 16))
            Text("Platforms")
                .font(.headline)
            Text(presenter.gameDetails?.platformString ?? "-")
                .font(.system(size: 16))
            Text("Developer")
                .font(.headline)
            Text(presenter.gameDetails?.developerString ?? "-")
                .font(.system(size: 16))
            Text("Publisher")
                .font(.headline)
            Text(presenter.gameDetails?.publisherString ?? "-")
                .font(.system(size: 16))
        }
        .padding(10)
    }
}

extension GameView {
    
    func openUrl(_ linkUrl: String?) {
        if let url = linkUrl, let link = URL(string: url) {
            UIApplication.shared.open(link)
        } else {
            showingAlert = true
        }
    }
    
}
