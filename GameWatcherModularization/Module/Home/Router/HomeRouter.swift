//
//  HomeRouter.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import Platform
import Core
import GameDetails

class HomeRouter {
    
    func makeDetailView(for platform: PlatformModel) -> some View {
        
        let useCase: Interactor<
            String,
            [GameDetailModel],
            GetGameDetailsRepository<
                GetGameDetailsLocaleDataSource,
                GetGameDetailsRemoteDataSource,
                GameDetailsTransformer<GameDetailTransformer>>
        > = Injection.init().provideGameDetails()
        
        let presenter = GetListPresenter(useCase: useCase)
        
        return DetailView(presenter: presenter, platform: platform)
    }
    
}
