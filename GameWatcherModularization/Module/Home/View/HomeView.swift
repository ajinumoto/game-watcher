//
//  HomeView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import Platform
import Core

struct HomeView: View {
    
    @ObservedObject var presenter: GetListPresenter<Any, PlatformModel, Interactor<Any, [PlatformModel], GetPlatformsRepository<GetPlatformsLocaleDataSource, GetPlatformsRemoteDataSource, PlatformTransformer>>>
    
    var body: some View {
        ZStack {
            if presenter.isLoading {
                loadingIndicator
            } else if presenter.isError {
                errorIndicator
            } else if presenter.list.isEmpty {
                emptyCategories
            } else {
                content
            }
        }
        .onAppear {
            if self.presenter.list.count == 0 {
                self.presenter.getList(request: nil)
            }
        }
        .navigationBarTitle(
            Text("Game Watcher 2.0"),
            displayMode: .automatic
        )
    }
    
}

extension HomeView {
    
    var loadingIndicator: some View {
        VStack {
            Text("Loading...")
            ActivityIndicator()
        }
    }
    
    var errorIndicator: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: presenter.errorMessage
        )
        .offset(y: 80)
    }
    
    var emptyCategories: some View {
        CustomEmptyView(
            image: "assetNoFavorite",
            title: "The game platform is empty"
        )
        .offset(y: 80)
    }
    
    var content: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ForEach(
                self.presenter.list,
                id: \.id
            ) { platform in
                ZStack {
                    linkBuilder(for: platform) {
                        PlatformRow(platform: platform)
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                .padding(8)
            }
        }
    }
    
    func linkBuilder<Content: View>(
        for platform: PlatformModel,
        @ViewBuilder content: () -> Content
    ) -> some View {
        
        NavigationLink(
            destination: HomeRouter().makeDetailView(for: platform)
        ) { content() }
    }
}

