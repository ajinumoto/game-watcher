//
//  PlatformRow.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import SDWebImageSwiftUI
import Platform

struct PlatformRow: View {
    
    var platform: PlatformModel
    var body: some View {
        ZStack(alignment: .center) {
            imageCategory
            content
                .padding()
                .background(Color.theme.background.opacity(0.7))
                .cornerRadius(10)
                .frame(width: UIScreen.main.bounds.width - 32)
        }
    }
    
}

extension PlatformRow {
    var imageCategory: some View {
        WebImage(url: URL(string: platform.imageBackground))
            .resizable()
            .indicator(.progress)
            .scaledToFill()
            .transition(.fade(duration: 0.5))
            .cornerRadius(10)
            .frame(height: 140)
            .clipped()
    }
    
    var content: some View {
        VStack(alignment: .leading) {
            Text(platform.name)
                .font(.title)
                .bold()
            HStack {
                Spacer()
                Text(platform.slug)
            }
            .font(.callout)
            .lineLimit(2)
        }
    }
}
