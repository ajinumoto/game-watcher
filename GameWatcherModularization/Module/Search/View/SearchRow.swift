//
//  SearchRow.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import SDWebImageSwiftUI
import GameDetails

struct SearchRow: View {
    
    var game: GameDetailModel
    
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                imageCategory
                content
                Spacer()
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 8)
            
            Divider()
                .padding(.leading)
        }
    }
    
}

extension SearchRow {
    
    var imageCategory: some View {
        WebImage(url: URL(string: game.backgroundImage))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFill()
            .frame(width: 120, height: 80)
            .clipped()
            .cornerRadius(10)
    }
    
    var content: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text(game.name)
                .font(.system(size: 20, weight: .semibold, design: .rounded))
                .lineLimit(3)
            
            Text(game.platformString)
                .font(.system(size: 16))
                .lineLimit(2)
            
            if !game.developerString.isEmpty {
                Text("From \(game.developerString)")
                    .font(.system(size: 14))
                    .lineLimit(2)
            }
            
        }.padding(
            EdgeInsets(
                top: 0,
                leading: 16,
                bottom: 16,
                trailing: 16
            )
        )
    }
    
}
