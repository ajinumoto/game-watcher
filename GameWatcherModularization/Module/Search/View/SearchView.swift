//
//  SearchView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import Core
import GameDetails

struct SearchView: View {
    
    @ObservedObject var presenter: SearchPresenter<GameDetailModel, Interactor<String, [GameDetailModel], SearchGameDetailRepository<GetGameDetailsRemoteDataSource, GameDetailsTransformer<GameDetailTransformer>>>>
    
    var body: some View {
        VStack {
            SearchBar(
                text: $presenter.keyword,
                onSearchButtonClicked: presenter.search
            )
            
            ZStack {
                if presenter.isLoading {
                    loadingIndicator
                } else if presenter.keyword.isEmpty {
                    emptyTitle
                } else if presenter.list.isEmpty {
                    emptyGames
                } else if presenter.isError {
                    errorIndicator
                } else {
                    ScrollView(.vertical, showsIndicators: false) {
                        ForEach(
                            self.presenter.list,
                            id: \.id
                        ) { game in
                            ZStack {
                                self.linkBuilder(for: game) {
                                    SearchRow(game: game)
                                }.buttonStyle(PlainButtonStyle())
                            }.padding(8)
                        }
                    }
                }
            }
            Spacer()
        }
        .navigationBarTitle(
            Text("Search Games"),
            displayMode: .automatic
        )
    }
}

extension SearchView {
    
    var loadingIndicator: some View {
        VStack {
            Text("Loading...")
            ActivityIndicator()
        }
    }
    
    var errorIndicator: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: presenter.errorMessage
        )
    }
    
    var emptyTitle: some View {
        CustomEmptyView(
            image: "assetJoystick",
            title: "Try type some game title..."
        )
    }
    var emptyGames: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: "Data not found"
        )
    }
    
    func linkBuilder<Content: View>(
        for game: GameDetailModel,
        @ViewBuilder content: () -> Content
    ) -> some View {
        
        NavigationLink(
            destination: DetailRouter().makeGameView(for: game)
        ) { content() }
    }
}
