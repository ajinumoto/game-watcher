//
//  FavoriteView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import Core
import GameDetails

struct FavoriteView: View {
    @ObservedObject var presenter: GetListPresenter<String, GameDetailModel, Interactor<String, [GameDetailModel], GetFavoriteGameDetailsRepository<GetFavoriteGameDetailLocaleDataSource, GameDetailsTransformer<GameDetailTransformer>>>>
    
    var body: some View {
        ZStack {
            
            if presenter.isLoading {
                loadingIndicator
            } else if presenter.isError {
                errorIndicator
            } else if presenter.list.count == 0 {
                emptyFavorites
            } else {
                content
            }
        }.onAppear {
            self.presenter.getList(request: nil)
        }.navigationBarTitle(
            Text("Favorite Games"),
            displayMode: .automatic
        )
    }
    
}

extension FavoriteView {
    var loadingIndicator: some View {
        VStack {
            Text("Loading...")
            ActivityIndicator()
        }
    }
    
    var errorIndicator: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: presenter.errorMessage
        )
    }
    
    var emptyFavorites: some View {
        CustomEmptyView(
            image: "assetNoFavorite",
            title: "Your favorite is empty"
        )
    }
    
    var content: some View {
        ScrollView(
            .vertical,
            showsIndicators: false
        ) {
            ForEach(
                self.presenter.list,
                id: \.id
            ) { game in
                ZStack {
                    self.linkBuilder(for: game) {
                        FavoriteRow(game: game)
                    }.buttonStyle(PlainButtonStyle())
                }
                
            }
        }
    }
    
    func linkBuilder<Content: View>(
        for game: GameDetailModel,
        @ViewBuilder content: () -> Content
    ) -> some View {
        
        NavigationLink(
            destination: DetailRouter().makeGameView(for: game)
        ) { content() }
    }
}
