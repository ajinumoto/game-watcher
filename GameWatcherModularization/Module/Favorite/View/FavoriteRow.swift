//
//  FavoriteRow.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import SDWebImageSwiftUI
import Core
import GameDetails

struct FavoriteRow: View {
    
    var game: GameDetailModel
    
    var body: some View {
        VStack {
            HStack(alignment: .top) {
                imageGame
                content
                Spacer()
            }
            .padding(.horizontal, 16)
            .padding(.vertical, 8)
            
            Divider()
                .padding(.leading)
        }
    }
    
}

extension FavoriteRow {
    
    var imageGame: some View {
        WebImage(url: URL(string: game.backgroundImage))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFill()
            .frame(width: 120, height: 80)
            .clipped()
            .cornerRadius(20)
    }
    
    var content: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text(game.name)
                .font(.system(size: 20, weight: .semibold, design: .rounded))
                .lineLimit(2)
            
            Text("Platforms: \(game.platformString)")
                .font(.system(size: 14))
                .lineLimit(2)
            
        }.padding(
            EdgeInsets(
                top: 0,
                leading: 16,
                bottom: 16,
                trailing: 16
            )
        )
    }
    
}
