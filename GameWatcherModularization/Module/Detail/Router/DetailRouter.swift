//
//  DetailRouter.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import Platform
import Core
import GameDetails

class DetailRouter {
    
    func makeGameView(for game: GameDetailModel) -> some View {
        let useCase: Interactor<
            String,
            GameDetailModel,
            GetGameDetailRepository<
                GetGameDetailsLocaleDataSource,
                GetGameDetailRemoteDataSource,
                GameDetailTransformer>
        > = Injection.init().provideGameDetail()
        
        let favoriteUseCase: Interactor<
            String,
            GameDetailModel,
            UpdateFavoriteGameDetailRepository<
                GetFavoriteGameDetailLocaleDataSource,
                GameDetailTransformer>
        > = Injection.init().provideUpdateFavorite()
        
        let presenter = GameDetailPresenter(gameDetailUseCase: useCase, favoriteUseCase: favoriteUseCase)
        
        return GameView(presenter: presenter, game: game)
    }
}
