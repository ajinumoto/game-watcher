//
//  DetailView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 29/12/22.
//

import SwiftUI
import SDWebImageSwiftUI
import Core
import GameDetails
import Platform

struct DetailView: View {
    @ObservedObject var presenter: GetListPresenter<String, GameDetailModel, Interactor<String, [GameDetailModel], GetGameDetailsRepository<GetGameDetailsLocaleDataSource, GetGameDetailsRemoteDataSource, GameDetailsTransformer<GameDetailTransformer>>>>
    
    var platform: PlatformModel
    
    var body: some View {
        ZStack {
            if presenter.isLoading {
                loadingIndicator
            } else if presenter.isLoading {
                errorIndicator
            } else {
                ScrollView(.vertical) {
                    VStack {
                        imageCategory
                        Group {
                            spacer
                            content
                            spacer
                        }
                    }
                }
            }
        }
        .onAppear {
            if self.presenter.list.count == 0 {
                self.presenter.getList(request: String(platform.id))
            }
        }
        
        .ignoresSafeArea()
        .navigationBarTitleDisplayMode(.inline)
    }
}

extension DetailView {
    var spacer: some View {
        Spacer()
    }
    
    var loadingIndicator: some View {
        VStack {
            Text("Loading...")
            ActivityIndicator()
        }
    }
    
    var errorIndicator: some View {
        CustomEmptyView(
            image: "assetSearchNotFound",
            title: presenter.errorMessage
        ).offset(y: 80)
    }
    
    var imageCategory: some View {
        WebImage(url: URL(string: self.platform.imageBackground))
            .resizable()
            .indicator(.activity)
            .transition(.fade(duration: 0.5))
            .scaledToFill()
            .overlay(
                LinearGradient(gradient: Gradient(colors: [Color.theme.background.opacity(0.3),
                                                           Color.theme.background]),
                               startPoint: .top,
                               endPoint: .bottom)
            )
            .overlay(
                VStack(alignment: .leading) {
                    Spacer()
                    HStack {
                        Text(self.platform.name)
                            .foregroundColor(.theme.accent)
                            .textCase(.uppercase)
                            .font(.title.bold())
                            .shadow(color: .theme.background, radius: 4)
                            .lineLimit(1)
                        Spacer()
                    }
                    Text(self.platform.slug)
                }
                    .offset(y: -20)
                    .padding()
                    .frame(width: UIScreen.main.bounds.width, height: 200.0)
            )
            .frame(width: UIScreen.main.bounds.width, height: 200.0)
    }
    
    var gamesHorizontal: some View {
        ScrollView(.horizontal) {
            HStack {
                ForEach(self.presenter.list, id: \.id) { game in
                    ZStack {
                        self.linkBuilder(for: game) {
                            GameRow(game: game)
                                .frame(width: 150, height: 150)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
    
    var description: some View {
        Text(String(platform.gamesCount))
            .font(.system(size: 15))
    }
    
    func headerTitle(_ title: String) -> some View {
        return Text(title)
            .font(.headline)
    }
    
    var content: some View {
        VStack(alignment: .leading) {
            if !presenter.list.isEmpty {
                headerTitle("Games on \(platform.name)")
                    .padding(.top)
                gamesHorizontal
            }
            spacer
            headerTitle("Realesed games on this Platform: ")
                .padding([.top, .bottom])
            description
        }
        .padding(10)
    }
    
    func linkBuilder<Content: View>(
        for game: GameDetailModel,
        @ViewBuilder content: () -> Content
    ) -> some View {
        
        NavigationLink(
            destination: DetailRouter().makeGameView(for: game)
        ) { content() }
    }
}

