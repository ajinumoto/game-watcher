//
//  ProfileView.swift
//  GameWatcherModularization
//
//  Created by Nexsoft on 02/01/23.
//

import SwiftUI

struct ProfileView: View {
    
    let rawgURL = URL(string: "https://rawg.io/apidocs")!
    let personalURL = URL(string: "https://www.instagram.com/ajinumoto")!
    
    var body: some View {
        List {
            developerView
            rawgView
            applicationSection
        }.navigationBarTitle(
            Text("About"),
            displayMode: .automatic
        )
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}

extension ProfileView {
    
    private var rawgView: some View {
        Section {
            VStack(alignment: .leading) {
                Image("rawg")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                Text("All of the data that is used in this app comes from a free API from RAWG! Some update may be slightly delayed")
                    .font(.callout)
                    .foregroundColor(.theme.accent)
            }
            .padding(.vertical)
            Link("Visit RAWG", destination: rawgURL)
                .foregroundColor(.blue)
        } header: {
            Text("RAWG")
        }
    }
    
    private var developerView: some View {
        Section {
            VStack(alignment: .center) {
                Image("avatar")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 100)
                    .clipShape(RoundedRectangle(cornerRadius: 20))
                Text("Adjie Satryo Pamungkas")
                    .font(.title3.bold())
                    .padding(.bottom)
                Text("This app was developed by Adjie Satryo. The project use Combine, publishers, subscribers, and data persistance.")
                    .font(.callout)
                    .foregroundColor(.theme.accent)
            }
            .padding(.vertical)
            Link("Visit My Personal Account", destination: personalURL)
                .foregroundColor(.blue)
        } header: {
            Text("Developer")
        }
    }
    
    private var applicationSection: some View {
        Section {
            Link("Terms of Service", destination: rawgURL)
                .foregroundColor(.blue)
            Link("Privacy Policy", destination: rawgURL)
                .foregroundColor(.blue)
            Link("Learn More", destination: personalURL)
                .foregroundColor(.blue)
        }
    header: {
        Text("Application")
    }
    }
}
