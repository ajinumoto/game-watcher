// swift-tools-version:5.4
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GameDetails",
    platforms: [.iOS(.v14)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GameDetails",
            targets: ["GameDetails"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(name: "Realm", url: "https://github.com/realm/realm-swift.git", from: "10.33.0"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", .upToNextMajor(from: "5.6.4")),
        .package(url: "https://gitlab.com/ajinumoto/game-watcher-core.git", .branchItem("main")),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "GameDetails",
            dependencies: [
                .product(name: "RealmSwift", package: "Realm"),
                .product(name: "Core", package: "game-watcher-core"),
                "Alamofire"
            ]),
        .testTarget(
            name: "GameDetailsTests",
            dependencies: ["GameDetails"])
    ]
)
