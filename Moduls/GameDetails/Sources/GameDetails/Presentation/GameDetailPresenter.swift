//
//  File.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Foundation
import Combine
import Core

public class GameDetailPresenter<GameDetailUseCase: UseCase, FavoriteUseCase: UseCase>: ObservableObject
where
GameDetailUseCase.Request == String, GameDetailUseCase.Response == GameDetailModel,
FavoriteUseCase.Request == String, FavoriteUseCase.Response == GameDetailModel {
    
    private var cancellables: Set<AnyCancellable> = []
    
    private let _gameDetailUseCase: GameDetailUseCase
    private let _favoriteUseCase: FavoriteUseCase
    
    @Published public var gameDetails: GameDetailModel?
    @Published public var errorMessage: String = ""
    @Published public var isLoading: Bool = false
    @Published public var isError: Bool = false
    
    public init(gameDetailUseCase: GameDetailUseCase, favoriteUseCase: FavoriteUseCase) {
        _gameDetailUseCase = gameDetailUseCase
        _favoriteUseCase = favoriteUseCase
    }
    
    public func getGameDetail(request: GameDetailUseCase.Request) {
        isLoading = true
        _gameDetailUseCase.execute(request: request)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    self.errorMessage = error.localizedDescription
                    self.isError = true
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { gameDetails in
                self.gameDetails = gameDetails
            })
            .store(in: &cancellables)
    }
    
    public func updateFavoriteGameDetail(request: FavoriteUseCase.Request) {
        _favoriteUseCase.execute(request: request)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure:
                    self.errorMessage = String(describing: completion)
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { gameDetails in
                self.gameDetails = gameDetails
            })
            .store(in: &cancellables)
    }
    
}
