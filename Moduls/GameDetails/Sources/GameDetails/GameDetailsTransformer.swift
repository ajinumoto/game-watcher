//
//  File.swift
//  
//
//  Created by Nexsoft on 26/12/22.
//

import Core

public struct GameDetailsTransformer<GameDetailMapper: Mapper>: Mapper
where
GameDetailMapper.Request == String,
GameDetailMapper.Response == GameDetailResponse,
GameDetailMapper.Entity == GameDetailEntity,
GameDetailMapper.Domain == GameDetailModel {
    
    public typealias Request = String
    public typealias Response = [GameDetailResponse]
    public typealias Entity = [GameDetailEntity]
    public typealias Domain = [GameDetailModel]
    
    private let _gameDetailMapper: GameDetailMapper
    
    public init(gameDetailMapper: GameDetailMapper) {
        _gameDetailMapper = gameDetailMapper
    }
    
    public func transformResponseToEntity(request: String?, response: [GameDetailResponse]) -> [GameDetailEntity] {
        return response.map { result in
            _gameDetailMapper.transformResponseToEntity(request: request, response: result)
        }
    }
    
    public func transformEntityToDomain(entity: [GameDetailEntity]) -> [GameDetailModel] {
        return entity.map { result in
            return _gameDetailMapper.transformEntityToDomain(entity: result)
        }
    }
}
