//
//  File 2.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Foundation

public struct GameDetailModel: Equatable, Identifiable {
    public let id: String
    public let name, gameDetailsDescription: String
    public let released: String
    public let backgroundImage: String
    public let website: String
    public var favorite: Bool = false
    
    public var developerString: String
    public var publisherString: String
    public var genreString: String
    public var platformString: String
    
    public init(id: String, name: String, gameDetailsDescription: String, released: String, backgroundImage: String, website: String, favorite: Bool, developerString: String, publisherString: String, genreString: String, platformString: String) {
        self.id = id
        self.name = name
        self.gameDetailsDescription = gameDetailsDescription
        self.released = released
        self.backgroundImage = backgroundImage
        self.website = website
        self.favorite = favorite
        self.developerString = developerString
        self.publisherString = publisherString
        self.genreString = genreString
        self.platformString = platformString
    }
}
