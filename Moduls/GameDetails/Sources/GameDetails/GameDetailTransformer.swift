//
//  File 2.swift
//
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import RealmSwift

public struct GameDetailTransformer: Mapper {
    
    public typealias Request = String
    public typealias Response = GameDetailResponse
    public typealias Entity = GameDetailEntity
    public typealias Domain = GameDetailModel
    
    public init() { }
    
    public func transformResponseToEntity(request: String?, response: GameDetailResponse) -> GameDetailEntity {
        
        let gameDetailEntitiy = GameDetailEntity()
        
        gameDetailEntitiy.id = String(response.id)
        gameDetailEntitiy.name = response.name
        gameDetailEntitiy.gameDetailsDescription = response.gameDetailsDescription ?? ""
        gameDetailEntitiy.released = response.released ?? ""
        gameDetailEntitiy.backgroundImage = response.backgroundImage ?? ""
        gameDetailEntitiy.website = response.website ?? ""
        gameDetailEntitiy.platformString = response.platformString ?? ""
        gameDetailEntitiy.platformID = response.platformID ?? ""
        gameDetailEntitiy.developerString = response.developerString ?? ""
        gameDetailEntitiy.publisherString = response.publisherString ?? ""
        gameDetailEntitiy.genreString = response.genreString ?? ""
        gameDetailEntitiy.favorite = response.favorite ?? false
        
        return gameDetailEntitiy
    }
    
    public func transformEntityToDomain(entity: GameDetailEntity) -> GameDetailModel {
        return GameDetailModel(
            id: entity.id,
            name: entity.name,
            gameDetailsDescription: entity.gameDetailsDescription,
            released: entity.released,
            backgroundImage: entity.backgroundImage,
            website: entity.website,
            favorite: entity.favorite,
            developerString: entity.developerString,
            publisherString: entity.publisherString,
            genreString: entity.genreString,
            platformString: entity.platformString)
    }
    
}
