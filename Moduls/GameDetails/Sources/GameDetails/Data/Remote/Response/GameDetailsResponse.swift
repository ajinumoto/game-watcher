//
//  File.swift
//  
//
//  Created by Nexsoft on 29/12/22.
//

public struct GameDetailsResponse: Codable {
    public let results: [GameDetailResponse]
}
