//
//  File.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Foundation

public struct GameDetailResponse: Codable {
    public let id: Int
    public let name: String
    public let gameDetailsDescription: String?
    public let released: String?
    public let backgroundImage: String?
    public let website: String?
    public let platforms: [GameDetailsPlatform]?
    public let developers, genres, publishers: [Developer]?
    public let esrbRating: EsrbRating?
    public var favorite: Bool?
    
    public var developerString: String? {
        guard let developers else { return nil }
        return developers.map { $0.name}.joined(separator: ", ") }
    public var publisherString: String? {
        guard let publishers else { return nil }
        return publishers.map { $0.name}.joined(separator: ", ") }
    public var genreString: String? {
        guard let genres else { return nil }
        return genres.map { $0.name}.joined(separator: ", ") }
    public var platformString: String? {
        guard let platforms else { return nil }
        return platforms.map { $0.platform.name }.joined(separator: ", ") }
    public var platformID: String? {
        guard let platforms else { return nil }
        return platforms.map { String($0.platform.id) }.joined(separator: ", ") }
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case gameDetailsDescription = "description"
        case released
        case backgroundImage = "background_image"
        case website
        case platforms, developers, genres, publishers, favorite
        case esrbRating = "esrb_rating"
    }
}

// MARK: - Developer
public struct Developer: Codable {
    public let id: Int?
    public let name: String
}

// MARK: - EsrbRating
public struct EsrbRating: Codable {
    public let id: Int?
    public let name: String
}

// MARK: - MetacriticPlatform
public struct MetacriticPlatform: Codable {
    public let metascore: Int?
    public let url: String?
    public let platform: MetacriticPlatformPlatform?
}

// MARK: - MetacriticPlatformPlatform
public struct MetacriticPlatformPlatform: Codable {
    public let platform: Int?
    public let name, slug: String
}

// MARK: - GameDetailsPlatform
public struct GameDetailsPlatform: Codable {
    public let platform: PlatformDetails
}

// MARK: - PlatformPlatform
public struct PlatformDetails: Codable {
    public let id: Int
    public let name, slug: String
    public let yearStart: Int?
    public let gamesCount: Int?
    public let imageBackground: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, slug
        case yearStart = "year_start"
        case gamesCount = "games_count"
        case imageBackground = "image_background"
    }
}

// MARK: - GameDetailsStore
public struct GameDetailsStore: Codable {
    public let id: Int
    public let url: String?
    public let store: Developer?
}
