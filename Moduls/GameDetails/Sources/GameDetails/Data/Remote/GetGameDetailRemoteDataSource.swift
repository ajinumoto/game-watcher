//
//  File 2.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine
import Alamofire
import Foundation

public struct GetGameDetailRemoteDataSource: DataSource {
    
    public typealias Request = String
    
    public typealias Response = GameDetailResponse
    
    private let _endpoint: String
    
    private var apiKey: String {
        guard let filePath = Bundle.main.path(forResource: "RAWG-Info", ofType: "plist") else {
            fatalError("Couldn't find file 'RAWG-Info.plist'.")
        }
        
        let plist = NSDictionary(contentsOfFile: filePath)
        guard let value = plist?.object(forKey: "API_KEY") as? String else {
            fatalError("Couldn't find key 'API_KEY' in 'RAWG-Info.plist'.")
        }
        return value
    }
    
    public init(endpoint: String) {
        _endpoint = endpoint
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        
        return Future<GameDetailResponse, Error> { completion in
            
            guard let request = request else { return completion(.failure(URLError.invalidRequest))}
            
            var components = URLComponents(string: _endpoint + String(request))!
            components.queryItems = [
                URLQueryItem(name: "key", value: self.apiKey)
            ]
            
            if let url = components.url {
                AF.request(url)
                    .validate()
                    .responseDecodable(of: GameDetailResponse.self) { response in
                        switch response.result {
                        case .success(let value):
                            let result = value
                            completion(.success(result))
                        case .failure:
                            completion(.failure(URLError.invalidResponse))
                        }
                    }
            }
        }
        .eraseToAnyPublisher()
    }
}
