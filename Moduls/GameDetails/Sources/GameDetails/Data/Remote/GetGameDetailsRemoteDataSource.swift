//
//  File.swift
//  
//
//  Created by Nexsoft on 29/12/22.
//

import Core
import Combine
import Alamofire
import Foundation

public struct GetGameDetailsRemoteDataSource : DataSource {
    
    public typealias Request = String
    
    public typealias Response = [GameDetailResponse]
    
    private let _endpoint: String
    
    public init(endpoint: String) {
        _endpoint = endpoint
    }
    
    public func execute(request: Request?) -> AnyPublisher<Response, Error> {
        
        return Future<[GameDetailResponse], Error> { completion in
            
            guard let request = request else { return completion(.failure(URLError.invalidRequest)) }
            
            if let url = URL(string: _endpoint + request) {
                AF.request(url)
                    .validate()
                    .responseDecodable(of: GameDetailsResponse.self) { response in
                        switch response.result {
                        case .success(let value):
                            completion(.success(value.results))
                        case .failure:
                            completion(.failure(URLError.invalidResponse))
                        }
                    }
            }
        }.eraseToAnyPublisher()
    }
}
