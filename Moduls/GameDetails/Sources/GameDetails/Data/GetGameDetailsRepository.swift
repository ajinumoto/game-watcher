//
//  File.swift
//  
//
//  Created by Nexsoft on 26/12/22.
//

import Core
import Combine

public struct GetGameDetailsRepository<
    GameDetailLocaleDataSource: LocaleDataSource,
    RemoteDataSource: DataSource,
    Transformer: Mapper>: Repository
where
GameDetailLocaleDataSource.Request == String,
GameDetailLocaleDataSource.Response == GameDetailEntity,
RemoteDataSource.Request == String,
RemoteDataSource.Response == [GameDetailResponse],
Transformer.Request == String,
Transformer.Response == [GameDetailResponse],
Transformer.Entity == [GameDetailEntity],
Transformer.Domain == [GameDetailModel] {
    
    public typealias Request = String
    public typealias Response = [GameDetailModel]
    
    private let _localeDataSource: GameDetailLocaleDataSource
    private let _remoteDataSource: RemoteDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: GameDetailLocaleDataSource,
        remoteDataSource: RemoteDataSource,
        mapper: Transformer) {
            
            _localeDataSource = localeDataSource
            _remoteDataSource = remoteDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<[GameDetailModel], Error> {
        return _localeDataSource.list(request: request)
            .flatMap { result -> AnyPublisher<[GameDetailModel], Error> in
                if result.count < 10 {
                    return _remoteDataSource.execute(request: request)
                        .map { _mapper.transformResponseToEntity(request: request, response: $0) }
                        .catch { _ in _localeDataSource.list(request: request) }
                        .flatMap {  _localeDataSource.add(entities: $0) }
                        .filter { $0 }
                        .flatMap { _ in _localeDataSource.list(request: request)
                                .map {  _mapper.transformEntityToDomain(entity: $0) }
                        }.eraseToAnyPublisher()
                } else {
                    return _localeDataSource.list(request: request)
                        .map { _mapper.transformEntityToDomain(entity: $0) }
                        .eraseToAnyPublisher()
                }
            }.eraseToAnyPublisher()
    }
}
