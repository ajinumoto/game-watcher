//
//  File.swift
//  
//
//  Created by Nexsoft on 26/12/22.
//

import Core
import Combine

public struct UpdateFavoriteGameDetailRepository<
    GameDetailLocaleDataSource: LocaleDataSource,
    Transformer: Mapper>: Repository
where
GameDetailLocaleDataSource.Request == String,
GameDetailLocaleDataSource.Response == GameDetailEntity,
Transformer.Request == String,
Transformer.Response == GameDetailResponse,
Transformer.Entity == GameDetailEntity,
Transformer.Domain == GameDetailModel {
    
    public typealias Request = String
    public typealias Response = GameDetailModel
    
    private let _localeDataSource: GameDetailLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: GameDetailLocaleDataSource,
        mapper: Transformer) {
            
            _localeDataSource = localeDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<GameDetailModel, Error> {
        
        guard let request = request else { fatalError("Request cannot be empty") }
        
        return _localeDataSource.get(id: request)
            .map { _mapper.transformEntityToDomain(entity: $0) }
            .eraseToAnyPublisher()
    }
}
