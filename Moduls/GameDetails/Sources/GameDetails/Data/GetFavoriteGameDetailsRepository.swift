//
//  File 4.swift
//
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine

public struct GetFavoriteGameDetailsRepository<
    GameDetailLocaleDataSource: LocaleDataSource,
    Transformer: Mapper>: Repository
where
GameDetailLocaleDataSource.Request == String,
GameDetailLocaleDataSource.Response == GameDetailEntity,
Transformer.Request == String,
Transformer.Response == [GameDetailResponse],
Transformer.Entity == [GameDetailEntity],
Transformer.Domain == [GameDetailModel] {
    
    public typealias Request = String
    public typealias Response = [GameDetailModel]
    
    private let _localeDataSource: GameDetailLocaleDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: GameDetailLocaleDataSource,
        mapper: Transformer) {
            
            _localeDataSource = localeDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<[GameDetailModel], Error> {
        return _localeDataSource.list(request: request)
            .map { _mapper.transformEntityToDomain(entity: $0) }
            .eraseToAnyPublisher()
    }
}
