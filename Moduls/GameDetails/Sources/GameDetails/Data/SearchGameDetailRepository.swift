//
//  File.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine

public struct SearchGameDetailRepository<
    RemoteDataSource: DataSource,
    Transformer: Mapper>: Repository
where
RemoteDataSource.Request == String,
RemoteDataSource.Response == [GameDetailResponse],
Transformer.Request == String,
Transformer.Response == [GameDetailResponse],
Transformer.Entity == [GameDetailEntity],
Transformer.Domain == [GameDetailModel] {
    
    public typealias Request = String
    public typealias Response = [GameDetailModel]
    
    private let _remoteDataSource: RemoteDataSource
    private let _mapper: Transformer
    
    public init(
        remoteDataSource: RemoteDataSource,
        mapper: Transformer) {
            
            _remoteDataSource = remoteDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<[GameDetailModel], Error> {
        
        return _remoteDataSource.execute(request: request)
            .map { _mapper.transformResponseToEntity(request: request, response: $0) }
            .map { _mapper.transformEntityToDomain(entity: $0) }
            .eraseToAnyPublisher()
    }
}
