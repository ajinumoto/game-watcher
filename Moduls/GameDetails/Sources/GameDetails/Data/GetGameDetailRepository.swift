//
//  File 5.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine

public struct GetGameDetailRepository<
    GameDetailLocaleDataSource: LocaleDataSource,
    RemoteDataSource: DataSource,
    Transformer: Mapper>: Repository
where
GameDetailLocaleDataSource.Request == String,
GameDetailLocaleDataSource.Response == GameDetailEntity,
RemoteDataSource.Request == String,
RemoteDataSource.Response == GameDetailResponse,
Transformer.Request == String,
Transformer.Response == GameDetailResponse,
Transformer.Entity == GameDetailEntity,
Transformer.Domain == GameDetailModel {
    
    public typealias Request = String
    public typealias Response = GameDetailModel
    
    private let _localeDataSource: GameDetailLocaleDataSource
    private let _remoteDataSource: RemoteDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: GameDetailLocaleDataSource,
        remoteDataSource: RemoteDataSource,
        mapper: Transformer) {
            
            _localeDataSource = localeDataSource
            _remoteDataSource = remoteDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<GameDetailModel, Error> {
        guard let request = request else { fatalError("Request cannot be empty") }
        
        return _localeDataSource.get(id: request)
            .catch { error in
                return _remoteDataSource.execute(request: request)
                    .map { _mapper.transformResponseToEntity(request: request, response: $0) }
                    .flatMap { _localeDataSource.add(entities: [$0])}
                    .filter { $0 }
                    .flatMap { _ in _localeDataSource.get(id: request)
                    }
                    .eraseToAnyPublisher()
            }
            .flatMap { result -> AnyPublisher<GameDetailModel, Error> in
                if result.gameDetailsDescription.isEmpty {
                    return _remoteDataSource.execute(request: request)
                        .map { _mapper.transformResponseToEntity(request: request, response: $0) }
                        .catch { _ in _localeDataSource.get(id: request) }
                        .flatMap { _localeDataSource.update(id: request, entity: $0) }
                        .filter { $0 }
                        .flatMap { _ in _localeDataSource.get(id: request)
                                .map { _mapper.transformEntityToDomain(entity: $0) }
                        }.eraseToAnyPublisher()
                } else {
                    return _localeDataSource.get(id: request)
                        .map { _mapper.transformEntityToDomain(entity: $0) }
                        .eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }
}
