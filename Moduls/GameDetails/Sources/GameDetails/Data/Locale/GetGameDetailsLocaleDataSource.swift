//
//  File.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine
import RealmSwift
import Foundation

public struct GetGameDetailsLocaleDataSource: LocaleDataSource {
    
    public typealias Request = String
    
    public typealias Response = GameDetailEntity
    
    private let _realm: Realm
    
    public init(realm: Realm) {
        _realm = realm
    }
    
    public func list(request: String?) -> AnyPublisher<[GameDetailEntity], Error> {
        return Future<[GameDetailEntity], Error> { completion in
            guard let request  = request else { return completion(.failure(DatabaseError.requestFailed)) }
            let gameDetails: Results<GameDetailEntity> = {
                _realm.objects(GameDetailEntity.self)
                    .filter("platformID CONTAINS %@", " \(request),")
                    .sorted(byKeyPath: "name", ascending: true)
            }()
            
            completion(.success(gameDetails.toArray(ofType: GameDetailEntity.self)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: [GameDetailEntity]) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try _realm.write {
                    for gameDetail in entities {
                        _realm.add(gameDetail, update: .all)
                    }
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
        }
        .eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<GameDetailEntity, Error> {
        return Future<GameDetailEntity, Error> { completion in
            
            let gameDetails: Results<GameDetailEntity> = {
                _realm.objects(GameDetailEntity.self)
                    .filter("id = '\(id)'")
            }()
            
            guard let gameDetail = gameDetails.first else {
                completion(.failure(DatabaseError.requestFailed))
                return
            }
            
            completion(.success(gameDetail))
            
        }
        .eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: GameDetailEntity) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            if let gameDetailEntity = {
                _realm.objects(GameDetailEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try _realm.write {
                        gameDetailEntity.setValue(entity.gameDetailsDescription, forKey: "gameDetailsDescription")
                        gameDetailEntity.setValue(entity.released, forKey: "released")
                        gameDetailEntity.setValue(entity.website, forKey: "website")
                        gameDetailEntity.setValue(entity.platformString, forKey: "platformString")
                        gameDetailEntity.setValue(entity.developerString, forKey: "developerString")
                        gameDetailEntity.setValue(entity.publisherString, forKey: "publisherString")
                        gameDetailEntity.setValue(entity.genreString, forKey: "genreString")
                    }
                    completion(.success(true))
                    
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
}
