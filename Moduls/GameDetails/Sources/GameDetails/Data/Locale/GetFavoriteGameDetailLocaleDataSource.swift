//
//  File.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Core
import Combine
import RealmSwift
import Foundation

public struct GetFavoriteGameDetailLocaleDataSource: LocaleDataSource {
    
    public typealias Request = String
    
    public typealias Response = GameDetailEntity
    
    private let _realm: Realm
    
    public init(realm: Realm) {
        _realm = realm
    }
    
    public func list(request: String?) -> AnyPublisher<[GameDetailEntity], Error> {
        return Future<[GameDetailEntity], Error> { completion in
            
            let gameDetailEntities = {
                _realm.objects(GameDetailEntity.self)
                    .filter("favorite = \(true)")
                    .sorted(byKeyPath: "name", ascending: true)
            }()
            completion(.success(gameDetailEntities.toArray(ofType: GameDetailEntity.self)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: [GameDetailEntity]) -> AnyPublisher<Bool, Error> {
        fatalError()
    }
    
    public func get(id: String) -> AnyPublisher<GameDetailEntity, Error> {
        
        return Future<GameDetailEntity, Error> { completion in
            if let gameDetailEntity = {
                _realm.objects(GameDetailEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try _realm.write {
                        gameDetailEntity.setValue(!gameDetailEntity.favorite, forKey: "favorite")
                    }
                    completion(.success(gameDetailEntity))
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }.eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: GameDetailEntity) -> AnyPublisher<Bool, Error> {
        fatalError()
    }
}
