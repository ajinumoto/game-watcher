//
//  File 2.swift
//  
//
//  Created by Nexsoft on 21/12/22.
//

import Foundation
import RealmSwift

public class GameDetailEntity: Object {
    
    @objc dynamic var id: String = ""
    @objc dynamic var name = ""
    @objc dynamic var gameDetailsDescription = ""
    @objc dynamic var released = ""
    @objc dynamic var backgroundImage = ""
    @objc dynamic var website = ""
    @objc dynamic var platformString = ""
    @objc dynamic var platformID = ""
    @objc dynamic var developerString = ""
    @objc dynamic var publisherString = ""
    @objc dynamic var genreString = ""
    @objc dynamic var favorite = false
    
    public override static func primaryKey() -> String? {
        return "id"
    }
}
