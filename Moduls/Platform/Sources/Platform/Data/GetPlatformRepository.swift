//
//  File.swift
//  
//
//  Created by Nexsoft on 30/12/22.
//

import Core
import Combine

public struct GetPlatformRepository<
    PlatformLocaleDataSource: LocaleDataSource,
    RemoteDataSource: DataSource,
    Transformer: Mapper>: Repository
where
PlatformLocaleDataSource.Request == String,
PlatformLocaleDataSource.Response == PlatformEntity,
RemoteDataSource.Request == String,
RemoteDataSource.Response == Platform,
Transformer.Request == String,
Transformer.Response == Platform,
Transformer.Entity == PlatformEntity,
Transformer.Domain == PlatformModel {
    
    public typealias Request = String
    public typealias Response = PlatformModel
    
    private let _localeDataSource: PlatformLocaleDataSource
    private let _remoteDataSource: RemoteDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: PlatformLocaleDataSource,
        remoteDataSource: RemoteDataSource,
        mapper: Transformer) {
            
            _localeDataSource = localeDataSource
            _remoteDataSource = remoteDataSource
            _mapper = mapper
        }
    
    public func execute(request: String?) -> AnyPublisher<PlatformModel, Error> {
        guard let request = request else { fatalError("Request cannot be empty") }
        
        return _localeDataSource.get(id: request)
            .catch { error in
                return _remoteDataSource.execute(request: request)
                    .map { _mapper.transformResponseToEntity(request: request, response: $0) }
                    .flatMap { _localeDataSource.add(entities: [$0])}
                    .filter { $0 }
                    .flatMap { _ in _localeDataSource.get(id: request)
                    }
                    .eraseToAnyPublisher()
            }
            .flatMap { result -> AnyPublisher<PlatformModel, Error> in
                if result.platformDescription.isEmpty {
                    return _remoteDataSource.execute(request: request)
                        .map { _mapper.transformResponseToEntity(request: request, response: $0) }
                        .catch { _ in _localeDataSource.get(id: request) }
                        .flatMap { _localeDataSource.update(id: request, entity: $0) }
                        .filter { $0 }
                        .flatMap { _ in _localeDataSource.get(id: request)
                                .map { _mapper.transformEntityToDomain(entity: $0) }
                        }.eraseToAnyPublisher()
                } else {
                    return _localeDataSource.get(id: request)
                        .map { _mapper.transformEntityToDomain(entity: $0) }
                        .eraseToAnyPublisher()
                }
            }
            .eraseToAnyPublisher()
    }
}
