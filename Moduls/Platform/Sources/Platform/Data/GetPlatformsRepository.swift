//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Core
import Combine
 
public struct GetPlatformsRepository<
    PlatformLocaleDataSource: LocaleDataSource,
    RemoteDataSource: DataSource,
    Transformer: Mapper>: Repository
where
    PlatformLocaleDataSource.Response == PlatformEntity,
    RemoteDataSource.Response == [Platform],
    Transformer.Response == [Platform],
    Transformer.Entity == [PlatformEntity],
    Transformer.Domain == [PlatformModel] {
  
    // 3
    public typealias Request = Any
    public typealias Response = [PlatformModel]
    
    private let _localeDataSource: PlatformLocaleDataSource
    private let _remoteDataSource: RemoteDataSource
    private let _mapper: Transformer
    
    public init(
        localeDataSource: PlatformLocaleDataSource,
        remoteDataSource: RemoteDataSource,
        mapper: Transformer) {
        
        _localeDataSource = localeDataSource
        _remoteDataSource = remoteDataSource
        _mapper = mapper
    }
    
    public func execute(request: Any?) -> AnyPublisher<[PlatformModel], Error> {
        return _localeDataSource.list(request: nil)
          .flatMap { result -> AnyPublisher<[PlatformModel], Error> in
            if result.isEmpty {
              return _remoteDataSource.execute(request: nil)
                    .map { _mapper.transformResponseToEntity(request: nil, response: $0) }
                .catch { _ in _localeDataSource.list(request: nil) }
                .flatMap { _localeDataSource.add(entities: $0) }
                .filter { $0 }
                .flatMap { _ in _localeDataSource.list(request: nil)
                  .map { _mapper.transformEntityToDomain(entity: $0) }
                }
                .eraseToAnyPublisher()
            } else {
              return _localeDataSource.list(request: nil)
                .map { _mapper.transformEntityToDomain(entity: $0) }
                .eraseToAnyPublisher()
            }
          }
          .eraseToAnyPublisher()
    }
}
