//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Foundation
import RealmSwift
 
public class PlatformEntity: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String = ""
    @objc dynamic var platformDescription: String = ""
    @objc dynamic var slug: String = ""
    @objc dynamic var gamesCount: Int = 0
    @objc dynamic var imageBackground: String = ""
    @objc dynamic var image: String = ""
    @objc dynamic var yearStart: Int = 0
    @objc dynamic var yearEnd: Int = 0
    
    public override static func primaryKey() -> String? {
        return "id"
    }
}
