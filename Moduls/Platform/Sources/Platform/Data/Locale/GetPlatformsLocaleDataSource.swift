//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Core
import Combine
import RealmSwift
import Foundation

public struct GetPlatformsLocaleDataSource: LocaleDataSource {
    
    public typealias Request = Any
    public typealias Response = PlatformEntity
    
    private let _realm: Realm
    
    public init(realm: Realm) {
        _realm = realm
    }
    
    public func list(request: Any?) -> AnyPublisher<[PlatformEntity], Error> {
        return Future<[PlatformEntity], Error> { completion in
            let platforms: Results<PlatformEntity> = {
                _realm.objects(PlatformEntity.self)
                    .sorted(byKeyPath: "name", ascending: false)
            }()
            completion(.success(platforms.toArray(ofType: PlatformEntity.self)))
            
        }.eraseToAnyPublisher()
    }
    
    public func add(entities: [PlatformEntity]) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            do {
                try _realm.write {
                    for platform in entities {
                        _realm.add(platform, update: .all)
                    }
                    completion(.success(true))
                }
            } catch {
                completion(.failure(DatabaseError.requestFailed))
            }
            
        }.eraseToAnyPublisher()
    }
    
    public func get(id: String) -> AnyPublisher<PlatformEntity, Error> {
        return Future<PlatformEntity, Error> { completion in
            
            let platforms: Results<PlatformEntity> = {
                _realm.objects(PlatformEntity.self)
                    .filter("id = '\(id)'")
            }()
            
            guard let platform = platforms.first else {
                completion(.failure(DatabaseError.requestFailed))
                return
            }
            
            completion(.success(platform))
            
        }
        .eraseToAnyPublisher()
    }
    
    public func update(id: String, entity: PlatformEntity) -> AnyPublisher<Bool, Error> {
        return Future<Bool, Error> { completion in
            if let platformEntity = {
                _realm.objects(PlatformEntity.self).filter("id = '\(id)'")
            }().first {
                do {
                    try _realm.write {
                        platformEntity.setValue(entity.platformDescription, forKey: "platformDescription")
                    }
                    completion(.success(true))
                    
                } catch {
                    completion(.failure(DatabaseError.requestFailed))
                }
            } else {
                completion(.failure(DatabaseError.invalidInstance))
            }
        }
        .eraseToAnyPublisher()
    }
}
