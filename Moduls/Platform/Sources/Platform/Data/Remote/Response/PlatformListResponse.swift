//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Foundation

// MARK: - Platform
public struct PlatformListResponse: Codable {
    public let count: Int
    public let next: String?
    public let previous: String?
    public let results: [Platform]
}

// MARK: - Result
public struct Platform: Codable {
    public let id: Int
    public let name, slug: String
    public let gamesCount: Int
    public let imageBackground: String
    public let platformDescription: String?
    public let image: String?
    public let yearStart: Int?
    public let yearEnd: Int?
    public let games: [Game]?

    enum CodingKeys: String, CodingKey {
        case id, name, slug
        case gamesCount = "games_count"
        case imageBackground = "image_background"
        case platformDescription = "description"
        case image
        case yearStart = "year_start"
        case yearEnd = "year_end"
        case games
    }
}

// MARK: - Game
public struct Game: Codable {
    public let id: Int
    public let slug, name: String
    public let added: Int
}
