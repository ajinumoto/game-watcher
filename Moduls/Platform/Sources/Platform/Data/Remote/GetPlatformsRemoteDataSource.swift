//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Core
import Combine
import Alamofire
import Foundation
 
public struct GetPlatformsRemoteDataSource: DataSource {
    public typealias Request = Any
    
    public typealias Response = [Platform]
    
    private let _endpoint: String
    
    public init(endpoint: String) {
        _endpoint = endpoint
    }
    
    public func execute(request: Any?) -> AnyPublisher<[Platform], Error> {
        return Future<[Platform], Error> { completion in
            
            if let url = URL(string: _endpoint) {
                AF.request(url)
                    .validate()
                    .responseDecodable(of: PlatformListResponse.self) { response in
                        switch response.result {
                        case .success(let value):
                            completion(.success(value.results))
                        case .failure:
                            completion(.failure(URLError.invalidResponse))
                        }
                    }
            }
        }
        .eraseToAnyPublisher()
    }
}
