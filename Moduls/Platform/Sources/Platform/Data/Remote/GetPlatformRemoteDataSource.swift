//
//  File.swift
//  
//
//  Created by Nexsoft on 30/12/22.
//

import Core
import Combine
import Alamofire
import Foundation
 
public struct GetPlatformRemoteDataSource: DataSource {
    public typealias Request = Any
    
    public typealias Response = Platform
    
    private let _endpoint: String
    
    public init(endpoint: String) {
        _endpoint = endpoint
    }
    
    public func execute(request: Any?) -> AnyPublisher<Platform, Error> {
        return Future<Platform, Error> { completion in
            
            if let url = URL(string: _endpoint) {
                AF.request(url)
                    .validate()
                    .responseDecodable(of: Platform.self) { response in
                        switch response.result {
                        case .success(let value):
                            completion(.success(value))
                        case .failure:
                            completion(.failure(URLError.invalidResponse))
                        }
                    }
            }
        }
        .eraseToAnyPublisher()
    }
}
