//
//  File.swift
//  
//
//  Created by Nexsoft on 20/12/22.
//

import Foundation

public struct PlatformModel: Equatable, Identifiable {
    
    public let id: Int
    public let name, slug: String
    public let gamesCount: Int
    public let imageBackground: String
    public let image: String?
    public let yearStart: Int?
    public let yearEnd: Int?
    public let platformDescription: String?

    public init(id: Int, name: String, slug: String, gamesCount: Int, imageBackground: String, image: String?, yearStart: Int?, yearEnd: Int?, platformDescription: String?) {
        self.id = id
        self.name = name
        self.slug = slug
        self.gamesCount = gamesCount
        self.imageBackground = imageBackground
        self.image = image
        self.yearStart = yearStart
        self.yearEnd = yearEnd
        self.platformDescription = platformDescription
    }
}
