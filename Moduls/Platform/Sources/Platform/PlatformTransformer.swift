import Core

public struct PlatformTransformer: Mapper {
    
    public typealias Request = Any
    public typealias Response = [Platform]
    public typealias Entity = [PlatformEntity]
    public typealias Domain = [PlatformModel]
    
    public init() {}
    
    public func transformResponseToEntity(request: Any?, response: [Platform]) -> [PlatformEntity] {
        return response.map { result in
            let newPlatform = PlatformEntity()
            newPlatform.id = result.id
            newPlatform.name = result.name
            newPlatform.slug = result.slug
            newPlatform.gamesCount = result.gamesCount
            newPlatform.imageBackground = result.imageBackground
            newPlatform.image = result.image ?? ""
            newPlatform.yearStart = result.yearStart ?? 0
            newPlatform.yearEnd = result.yearEnd ?? 0
            newPlatform.platformDescription = result.platformDescription ?? ""
            return newPlatform
        }
    }
    
    public func transformEntityToDomain(entity: [PlatformEntity]) -> [PlatformModel] {
        return entity.map { result in
            return PlatformModel(
                id: result.id,
                name: result.name,
                slug: result.slug,
                gamesCount: result.gamesCount,
                imageBackground: result.imageBackground,
                image: result.image,
                yearStart: result.yearStart,
                yearEnd: result.yearEnd,
                platformDescription: result.platformDescription)
        }
    }
}
